package se.experis.DiaryApp.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se.experis.DiaryApp.Models.Entry;
import se.experis.DiaryApp.Repositories.EntryRepository;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ThymeController {

    @Autowired
    private EntryRepository entryRepository;

    @GetMapping("/")
    public String getEntry(HttpServletRequest request, Model model) {
        List<Entry> entries = entryRepository.findAllByOrderByPublishedOnDesc();
        model.addAttribute("allentries", entries);
        return "allentries";
    }

    @GetMapping("/add-entry")
    public String addEntry(Model model) {
        return "addentry";
    }

    @GetMapping("/edit-view/{id}")
    public String editController(@PathVariable("id") String id, Model model) {
        Entry entry= entryRepository.findById(Integer.parseInt(id)).get();
        model.addAttribute("entry", entry);
        return "editentry";
    }

}