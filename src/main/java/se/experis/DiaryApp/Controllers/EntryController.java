package se.experis.DiaryApp.Controllers;

import com.fasterxml.jackson.core.io.JsonEOFException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.DiaryApp.Models.CommonResponse;
import se.experis.DiaryApp.Models.Entry;
import se.experis.DiaryApp.Repositories.EntryRepository;
import se.experis.DiaryApp.Utils.Command;
import se.experis.DiaryApp.Utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


@RestController
public class EntryController {

    @Autowired
    private EntryRepository entryRepository;


    @GetMapping("/entry/{id}")
    public ResponseEntity<CommonResponse> getEntryById(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if (entryRepository.existsById(id)) {
            cr.data = entryRepository.findById(id);
            cr.message = "Entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Entry not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/add")
    public ResponseEntity<Entry> addEntry(HttpServletRequest request, @RequestBody Entry entry) {
        entry = entryRepository.save(entry);
        System.out.println("New entry with id: " + entry.id);
        HttpStatus resp = HttpStatus.CREATED;
        return new ResponseEntity<>(entry, resp);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEntry(HttpServletRequest request, @PathVariable Integer id) {
        String message = "";
        HttpStatus resp;

        if (entryRepository.existsById(id)) {
            entryRepository.deleteById(id);
            System.out.println("Deleted entry with id: " + id);
            message = "GONE!";
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found with id: " + id);
            message = "FAIL";
            resp = HttpStatus.OK;
        }
        return new ResponseEntity<>(message, resp);
    }

    @PatchMapping("/edit/{id}")
    public ResponseEntity<Entry> editEntry(HttpServletRequest request, @RequestBody Entry newEntry, @PathVariable Integer id) {
        Entry entry = null;
        HttpStatus resp = null;

        if (entryRepository.existsById(id)) {
            Optional<Entry> entryRepo = entryRepository.findById(id);
            entry = entryRepo.get();
        }

        if (newEntry.title != null) {
            entry.title = newEntry.title;
        }
        if (newEntry.text != null) {
            entry.text = newEntry.text;
        }
            if (newEntry.imageURL != null) {
                entry.imageURL = newEntry.imageURL;
            }


            entryRepository.save(entry);
            System.out.println("Updated entry with id: " + entry.id);
            resp = HttpStatus.OK;
            return new ResponseEntity<>(entry, resp);
        }
    }

