package se.experis.DiaryApp.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.DiaryApp.Models.Entry;
import java.util.List;

public interface EntryRepository extends JpaRepository<Entry, Integer> {
    Entry getById(int id);
    List<Entry> findAllByOrderByPublishedOnDesc();
}


